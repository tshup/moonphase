

# MoonPhase #
MoonPhase is a free J2ME application that displays the current moon phase on your mobile phone. It can also display the dates of all new/full moon phases for a given year. The program works on MIDP2 compatible Java-enabled phones, and does not require an Internet connection.

### Features ###
The main screen displays the current moon phase, moon age and percentage of the moon progress.
The percentage value represents the illuminated fraction of the Moon's disc as a value from -99 to +99.

The value is negative when the Moon wanes, and positive otherwise.

The New Moon phase is around 0% and the Full Moon phase is around the +/-99%.

To display the moon phase for a specific date, use the calendar control on the main screen.

### Installing directly to your mobile device ###
To install MoonPhase directly from your mobile device:
  1. Open www.getjar.com with the built-in browser on your J2ME mobile phone.
  1. Click **Quick Download**.
  1. Enter download code **18228**, and click **Next**.
  1. Click **Download to Mobile**.
  1. Click **Ok** to start dowloading.
After the download completes, MoonPhase will be installed on your device.

Note: Installing directly from a mobile device requires a mobile data plan (3G, EDGE, GPRS, CSD, etc).

### Installing via a PC ###
To install from a desktop computer, download [MoonPhase.jar](http://moonphase.googlecode.com/files/MoonPhase.jar) and [MoonPhase.jad](http://moonphase.googlecode.com/files/MoonPhase.jad) to your desktop computer and then transfer them to your device via a cable, infrared or Bluetooth. For some mobile phones the .jad file might not be required. Alternatively, use the [Download](http://www.getjar.com/mobile/18228/moonphase/) option on the [getjar](http://www.getjar.com/mobile/18228/moonphase/) page.

MoonPhase uses portions of code from the MoonTool application by John Walker, the Stellafane Moon Phase Calculator by by Ken Slater, and the MoonCalculator by Angus McIntyre.


[![](http://img.brothersoft.com/v1/mobile/images/mobile/mobile_awards.gif)](http://www.brothersoft.com/mobile/moonphase-6721.html)
